require 'spec_helper'

describe 'postfix::maincf', :type => :define do
  let :precondition do
    "class { '::postfix': }"
  end
  
 let (:facts) do
    {
      :osfamily => 'Debian',
      :caller_module_name => 'postfix',
      :module_name => 'postfix'
    }
 end

  let :title do
    'test_config_key'
  end
 
  describe 'ensure config present' do
    let (:params) {{ :value => 'test_config_value', :ensure => 'present' }}
    it { should compile.with_all_deps }
    it { should contain_file_line('test_config_key').with_line('test_config_key = test_config_value') }
    it { should contain_file_line('test_config_key').with_ensure('present') }
  end

  describe 'ensure config absent' do
    let (:params) {{ :value => 'test_config_value', :ensure => 'absent' }}
    it { should compile.with_all_deps }
    it { should contain_file_line('test_config_key').with_line('test_config_key = test_config_value') }
    it { should contain_file_line('test_config_key').with_ensure('absent') }
  end
end

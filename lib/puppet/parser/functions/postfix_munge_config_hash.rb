module Puppet::Parser::Functions
  newfunction(:postfix_munge_config_hash, :type => :rvalue, :arity => 1, :doc => <<-EOS
  Takes a hash of keys and values and converts the values such that the hash is appriate
  for passing to create_resources() with type ::postfix::maincf.
EOS
) do |args|
    unless 1 == args.length and args[0].is_a? Hash
      raise Puppet::ParseError
    end

    config_hash = args[0]
    new_config_hash = {}

    config_hash.each do |k, v|
      new_config_hash[k] = { 'value' => v }
    end

    debug("Generated hash: #{new_config_hash}")
    new_config_hash
  end
end

# == Define postfix::maincf
#
# Inject a configuration parameter in Postfix config.
#
# [*ensure*]
#   valid: present|absent
#
# [*value*]
#   Optional value for the parameter. Optional because the folling
#   is perfectly fine in a postfix config:
#     relayhost =
#
define postfix::maincf(
  $ensure = present,
  $value = undef
) {

  include ::postfix

  if ($caller_module_name != $module_name) {
    fail("This define is private. Cannot declare from ${caller_module_name}!")
  }

  validate_re($ensure, '^(present|absent)$')

  file_line { $name:
    ensure => $ensure,
    path   => $::postfix::config,
    line   => "${name} = ${value}",
    match  => "^\s*${name}\s+=.*$",
  }
}

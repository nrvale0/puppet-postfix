[![Build Status](https://travis-ci.org/nrvale0/puppet-postfix.png)](https://travis-ci.org/nrvale0/puppet-postfix)

nrvale0-postfix
---------------

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with postfix](#setup)
    * [What postfix affects](#what-postfix-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with postfix](#beginning-with-postfix)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Simple module for installing and running the Postfix MTA on RedHat-based
and Debian-based systems.

## Module Description

The module installs the Postfix MTA from vendor packages on supported platforms
and provides the ability to override package name and service init state.

The module is appropriate for forwarding MTA setup and basic inbound MTA. It 
does not currently expose an API which allows complicated mail filtering, TLS,
nor vhosted setups. Pull requests encouraged.

## Setup

### What postfix affects

* by default, installs from vendor Postfix packages
* makes modifications to /etc/postfix/main.cf

### Beginning with postfix

```ShellSession
sudo puppet module install nrvale0-postfix
```

## Usage

```puppet
include ::postfix
```

```puppet
class { '::postfix':
  relayhost => 'mail.myorg.private',
  myorigin => $::fqdn,
}
```

Note there are no *required* parameters to class postfix.

## Reference

* 'relayhost' and 'myorigin' are injected into main.cf using Puppet resource
type file_line from puppetlabs/stdlib.

## Limitations

* current OS support:
 * Ubuntu 12.04, 14.04
 * Debian 7
 * RedHat/CentOS 6

* upcoming OS support
 * Ubuntu 13.10
 * RedHat/CentOS 7

## Development

https://github.com/nrvale0/puppet-postfix

### License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

### About the author

Nathan Valentine - [nrvale0@gmail.com](mailto:nrvale0@gmail.com) | [nathan@puppetlabs.com](mailto:nathan@puppetlabs.com)
[http://about.me/nrvale0](http://about.me/nrvale0)

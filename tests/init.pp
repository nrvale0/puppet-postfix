class { '::postfix':
  relayhost => 'somerelay',
  myorigin  => $::hostname,
  configs   => { 'foo' => 'bar', 'baz' => 'shiz' },
}
